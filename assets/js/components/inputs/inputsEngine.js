/*
    INPUT CLASS TO RUN THE KEYBOARD EVENTS
    DO NOT TOUCH UNLESS YOU KNOW WHAT YOU'RE DOING!!
*/

export class Input {
    constructor(keys, callbackKeyDown, callbackKeyUp) {
        //console.log(keys);
        this.keys = keys;
        this.pressed = false;

        keys.forEach(key => {

            window.addEventListener("keydown", (keyPressed) => {

                if (keyPressed.key == key) {
                    
                    if (this.pressed) { return; };
                    this.pressed = true;

                    if (typeof callbackKeyDown == 'function') {
                        callbackKeyDown();
                    }

                };
            });
            window.addEventListener("keyup", (keyReleased) => {

                if (keyReleased.key == key) {

                    // console.log("key released!");
                    if (this.pressed) {
                        this.pressed = false;
                        // console.log(this);
                        if (typeof callbackKeyUp == 'function') {
                            //console.log("KeyUp");
                            callbackKeyUp();
                        }
                    } else {
                        console.log("wow you released the key without even pushing it");
                    }
                }
            });
        });
    };
};
