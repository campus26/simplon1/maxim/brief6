/*
    Example level
    ALL THE OBJECTS NEEDED INTO THE LEVEL APART FROM THE PLAYER
*/

export const level = {
    name: "Niveau 1",
    bgcolor: "#fafafa",
    gravity: 1,
    entities: [
        {
            // Ground
            shape: "rectangle",
            x: 1920/2,
            y: 580,
            width: 1920,
            height: 100,
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Door
            shape: "rectangle",
            x: 1920 - 25, // minus half door width 
            y: 1080 - 540/2, // minus half ground height
            width: 50,
            height: 540,
            isStatic: true,
            fillStyle: "black",
            strokeStyle: "white"
        },
        {
            // Left wall
            shape: "rectangle",
            x: 0 - 5, // minus half left wall width to hide the wall
            y: 0,
            width: 100,
            height: window.innerHeight * 2 - 10, // minus ground height
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Top wall
            shape: "rectangle",
            x: 1920/2,
            y: 0 - 5,
            width: 1920,
            height: 100,
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        // {
        //     // Ball - decoration
        //     shape: "circle",
        //     x: 460,
        //     y: 10,
        //     radius: 40,
        //     fillStyle: "pink",
        //     strokeStyle: "black"
        // },
        // {
        //     // Rectangle - decoration
        //     shape: "rectangle",
        //     x: 400,
        //     y: 200,
        //     width: 80,
        //     height: 80,
        //     fillStyle: "red",
        //     strokeStyle: "black"
        // }
    ]
}
