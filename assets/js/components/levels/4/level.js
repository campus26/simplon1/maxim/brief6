/* 
    Nadia's level
*/

export const level = {
    name: "Nadia's world",
    bgcolor: "#fafafa", // not used for now
    gravity: 2,
    entities: [
        {
            // Bridge - https://github.com/liabru/matter-js/blob/master/examples/bridge.js#L49
            shape: "bridge"
        },
        {
            // Ground
            shape: "rectangle",
            x: 0,
            y: window.innerHeight + 5, // plus half height to hide the ground
            width: window.innerWidth * 2,
            height: 10, // also used for left and right walls height and the door
            isStatic: true, // isStatic: true = gravity doesn't work on it
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Door
            shape: "rectangle",
            x: window.innerWidth - 25, // minus half door width 
            y: window.innerHeight - 5, // minus half ground height
            width: 50,
            height: 540,
            isStatic: true,
            fillStyle: "black",
            strokeStyle: "white"
        },
        {
            // Left wall
            shape: "rectangle",
            x: 0 - 5, // minus half left wall width to hide the wall
            y: 0,
            width: 10,
            height: window.innerHeight * 2 - 10, // minus ground height
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Top wall
            shape: "rectangle",
            x: 0,
            y: 0 - 5, // minus half top wall hright to hide the wall
            width: window.innerWidth * 2,
            height: 10,
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Right wall
            shape: "rectangle",
            x: window.innerWidth + 5, // minus half right width to hide the wall
            y: 0,
            width: 10,
            height: window.innerHeight - 5, // minus ground height
            isStatic: true,
            fillStyle: "chartreuse",
            strokeStyle: "black"
        },
        {
            // Plateforme
            shape: "rectangle",
            x: window.innerWidth / 2, // center it,
            y: window.innerHeight / 3, // a bit higher than centered
            width: 300,
            height: 30,
            isStatic: true,
            fillStyle: "black",
            strokeStyle: "black"
        }/*,
        {
            // Ball - decoration
            shape: "circle",
            x: 460,
            y: 10,
            radius: 40,
            fillStyle: "pink",
            strokeStyle: "black"
        },
        {
            // Rectangle - decoration
            shape: "rectangle",
            x: 400,
            y: 200,
            width: 80,
            height: 80,
            fillStyle: "red",
            strokeStyle: "black"
        }*/
    ]
}