/* 
    CREATES THE PLAYER AND THE WAY TO CONTROL IT (THEREFORE THE CAMERA MOVEMENTS)
*/
import { engine } from '../../gameEngine.js';
import { keyboard } from '../../inputs/inputsManager.js';

export let playerX = 380; // export to use it for the camera's starting point
export let playerY = 100; // export to use it for the camera's starting point
let playerRadius = 40;

let angularVelocityToSet = 0;
let jumpVelocity = 0;

/*
    Creates the player
*/

export let player = Bodies.circle(playerX, playerY, playerRadius, {  //x,y,taille
    //friction:0,
    render: { // options
        fillStyle: "chartreuse",
        strokeStyle: "black"
    }
});


/*
    Controls the player and the camera (as it follows the player)
*/

Events.on(engine, 'beforeUpdate', function (event) {

    // Player movements
    if (keyboard.up.pressed) {
        jumpVelocity++;
        Body.setVelocity(player, { x: player.velocity.x, y: -15 });
    }

    keyboard.left.pressed ? angularVelocityToSet = -0.2 : false;
    keyboard.right.pressed ? angularVelocityToSet = 0.2 : false;
    keyboard.down.pressed ? angularVelocityToSet = 0 : false;

    if (keyboard.left.pressed || keyboard.right.pressed || keyboard.down.pressed) {
        Body.setAngularVelocity(player, angularVelocityToSet);
    }

});
