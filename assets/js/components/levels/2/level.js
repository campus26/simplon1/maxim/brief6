/* 
    Marnoche's level
*/
export const level = {
    name: "Marnoche's world",
    bgcolor: "#fafafa",
    gravity: 1,
    entities: [ 

        {
            shape: "tetrisName",
            isStatic: true,
            x: -648,
            y: 45,
            width: 80,
            height: 80,
            fillStyle: "chartreuse", 
        },
        {
            shape: "tetrisNameB",
            isStatic: true,
            x: -405,
            y: 369,
            width: 80,
            height: 80,
            fillStyle: "#AA15FF", 
        },
        {
            // Left wall
            shape: "wallI",
            x: 0 - 891, // minus half left wall width to hide the wall
            y: 0 -36,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        {
            // Left wall
            shape: "wallI",
            x: 0 - 891, // minus half left wall width to hide the wall
            y: 369,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        
        {
            // Left wall
            shape: "wallI",
            x: 0 - 81, // minus half left wall width to hide the wall
            y: 0 - 36,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        
        {
            // Left wall
            shape: "wallI",
            x: 0 - 81, // minus half left wall width to hide the wall
            y: 369,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 0-891,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: -243,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 405,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 1053,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 1701,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 2349,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 2997,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 3645,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "wallI-Sleep",
            isStatic: true,
            x: 4293,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            shape: "tetrisI-Sleep",
            isStatic: true,
            x: 4941,
            y: 855,
            width: 80,
            height: 80,
            fillStyle: "gray",
             
        },
        {
            // right wall
            shape: "wallI",
            x: 5184, // minus half left wall width to hide the wall
            y: 369,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        
        {
            // right wall
            shape: "wallI",
            x: 5184, // minus half left wall width to hide the wall
            y: 0 - 117,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        
        {
            // Rectangle - decoration cyan
            shape: "tetrisI",
            x: 0,
            y: 531,
            isStatic: true,
            width: 80,
            height: 80,
            fillStyle: "cyan",
            strokeStyle: "black",
            lineWidth: 5
        },
        {
            // Rectangle - decoration cyan
            shape: "tetrisI",
            x: 0,
            y: -36,
            isStatic: true,
            width: 80,
            height: 80,
            fillStyle: "cyan",
            strokeStyle: "black",
            lineWidth: 5
        },
        {
            // Rectangle - decoration violet
            shape: "tetrisT-upside-down",
            x: 324,
            y: 774,
            isStatic: true,
            width: 80,
            height: 80,
            fillStyle: "#AA15FF",
            strokeStyle: "black",
            lineWidth: 5
        },
        { // decoration yellow
            shape: "tetrisO",
            isStatic: true,
            x: 486,
            y: 693,
            width: 80,
            height: 80,
            fillStyle: "yellow",
             
        },

        { // decoration blue
            shape: "tetrisL-Left",
            isStatic: true,
            x: 567,
            y: 774,
            width: 80,
            height: 80,
            fillStyle: "blue",
             
        },

        { // decoration orange
            shape: "tetrisL-Right",
            isStatic: true,
            x: 648,
            y: 774,
            width: 80,
            height: 80,
            fillStyle: "orange",
             
        },
        {
            shape: "tetrisZ",
            isStatic: true,
            x: 810,
            y: 693,
            width: 80,
            height: 80,
            fillStyle: "red",
             
        },
        {
            shape: "tetrisZ-RightMove",
            isStatic: true,
            x: 1377,
            y: 490,
            width: 80,
            height: 80,
            fillStyle: "chartreuse",
            speed: 0.002
        },
        
        {
            shape: "tetrisI-Sleep",
            isStatic: true,
            x: 1539,
            y: 61,
            width: 80,
            height: 80,
            fillStyle: "cyan",
             
        },

        {
            shape: "tetrisLBlue",
            isStatic: true,
            x: 162,
            y: 45,
            width: 80,
            height: 80,
            fillStyle: "blue",
             
        },
        
        {
            shape: "tetrisT",
            isStatic: true,
            x: 1539,
            y: 774,
            width: 80,
            height: 80,
            fillStyle: "#AA15FF",
            speed: 0.001
            
        },
        {
            shape: "tetrisTT",
            isStatic: true,
            x: 1964,
            y: 556,
            width: 80,
            height: 80,
            fillStyle: "blue",
            speed: 0.003
        },
        {
            shape: "tetrisI-Sleep",
            isStatic: true,
            x: 1863,
            y: 680,
            width: 80,
            height: 80,
            fillStyle: "cyan",
        },
        {
            shape: "tetrisZ",
            isStatic: true,
            x: 2025,
            y: 0-36,
            width: 80,
            height: 80,
            fillStyle: "red",
             
        },
        {
            // right wall
            shape: "wallI",
            x: 2268, // minus half left wall width to hide the wall
            y: 288,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        
        {
            // right wall
            shape: "wallI",
            x: 2268, // minus half left wall width to hide the wall
            y: 0 - 117,
            width: 80,
            height: 80, // minus ground height
            isStatic: true,
            fillStyle: "gray",
            strokeStyle: "black"
        },
        {
            shape: "tetrisT",
            isStatic: true,
            x: 2430,
            y: 640,
            width: 80,
            height: 80,
            fillStyle: "#AA15FF",
            speed: 0.002
        },
        {
            shape: "tetrisLOrange",
            isStatic: true,
            x: 2835,
            y: 369,
            width: 80,
            height: 80,
            fillStyle: "orange",
        },
        { 
            shape: "tetrisO",
            isStatic: true,
            x: 2916,
            y: 774,
            width: 80,
            height: 80,
            fillStyle: "yellow",
        },
        
        {
            shape: "tetrisZ-move",
            isStatic: true,
            x: 3380,
            y: 470,
            width: 80,
            height: 80,
            fillStyle: "red",
            speed: 0.002
        },
        {
            shape: "tetrisZ-move",
            isStatic: true,
            x: 3380,
            y: 20,
            width: 80,
            height: 80,
            fillStyle: "red",
            speed: 0.002
        },
        {
            shape: "tetrisLBlue",
            isStatic: true,
            x: 3726,
            y: 369,
            width: 80,
            height: 80,
            fillStyle: "blue",
        },

        {
            shape: "bridge",
        },

        {
            shape: "tetrisZ-Right",
            isStatic: true,
            x: 4941,
            y: 288,
            width: 80,
            height: 80,
            fillStyle: "chartreuse",
        },
        {
            shape: "rectangleEnd",
            x: 5103,
            y: 85, // plus half height to hide the ground
            width: 80,
            height: 162, // also used for left and right walls height and the door
            isStatic: true, // isStatic: true = gravity doesn't work on it
            isSensor: true,
            fillStyle: "rgba(148, 216, 233, 0.8)",
            strokeStyle: "black"
        },

        {
            shape: "tetrisZ-Right",
            isStatic: true,
            x: 4941,
            y: 288,
            width: 80,
            height: 80,
            fillStyle: "chartreuse",
        },
        {
            shape: "rectangleEnd",
            x: 5103,
            y: 85, // plus half height to hide the ground
            width: 80,
            height: 162, // also used for left and right walls height and the door
            isStatic: true, // isStatic: true = gravity doesn't work on it
            isSensor: true,
            fillStyle: "rgba(148, 216, 233, 0.8)",
            strokeStyle: "black"
        },
    ]
}
