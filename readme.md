# How it works
![How it works](https://gitlab.com/campus26/simplon1/maxim/brief6/-/raw/master/assets/images/howItWorks.png "How it works")

# Déroulement de la création du jeu _(notes pour la présentation finale)_

## BUT DU JEU (heee) : ON S'AMUSE et FAIT UN CODE PROPRE (plus important que le rendu final)

1. Il faut qu'on découpe le plus possible le projet **jusqu'à arriver à des étapes qu'on maitrise**
2. Puis se les assigner via le board GitLab
3. Puis qu'on créé **une branche pour chaque mini étape**
4. Et enfin, quand chaque étape est fonctionnelle, MAJ du board GitLab et merge sur master

## Etapes effectuées, notes pour rendu/explications finales

### 24 février
1. Créer les **fichiers de base** et les PUSH !
2. Définir les grandes étapes sur le **board GitLab**

### 25 février - 3 mars
1. Se renseigner chacun de notre côté sur **CANVAS** et **comprendre comment ça marche**
- choix *source Max* : http://sdz.tdct.org/sdz/creer-un-mini-rpg-en-javascript-avec-canvas.html
- choix *source Nadia* : https://developer.mozilla.org/fr/docs/Web/API/Canvas_API/Tutoriel_canvas + https://developer.mozilla.org/fr/docs/Web/API/Canvas_API/Tutorial/Advanced_animations
- choix *source Marnoche* : https://developer.mozilla.org/fr/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript
2. **Phase de tests**
- *Max* : map (sprite) + camera + personnage (sprite) sous différents angles + collision murs
- *Marnoche* : mouvements personnage : déplacement, collision murs, angles de vue, sauts, gravité... 
- *Nadia* : comprendre comment marchent les modules (imports/export) + adapter code Antho de div à canvas + collisions canvas et rebonds *(collision avec brick satique = fail)*

### 4 mars
1. Tous ok pour que **Nadia** passe sur la partie "**chef d'orchestre**" _(parce que quand ça veut pas, ça veut pas et on n'a pas le temps de voir venir)_ :
- **adaptation** des codes de Max et Marnoche à une implémentation en modules au fur et à mesure qu'ils seront prêts
- commentaires, facilité, compréhension et lisibilité du code
- prise de notes (ici) pour compte rendu final
- dispo pour aide/recherche
2. Antho : **introduction à Matter.js** => ça booste et nous lance dans le concret
3. **Mise en commun** de notre compréhension et nos visions
4. Choix rapide car on a été d'accord, sur la **direction à prendre** et le **thème du jeu**
- plateforme thème simple avec des formes, sans textures
- type escape game sur déjà 1 "niveau" puis un second et un 3ème avec contrainte de temps en guise de score (rapidité)
- plusieurs idées lancées quant aux détails : scénario, perso, énigme, énigme + n
5. Division des grandes étapes et assignation : **"Atomic run"** : en fait ça s'est fait naturellement, on est pas mal complémentaires !
- chacun est responsable de sa propre subdivision de son étape en étapes plus petites et réalisables
#### BUT : ne plus coder !! Faire un puzzle :)
1. Chercher dans Matter
2. Copier
3. Coller en local
4. Assembler
5. Jouer ! On se fait plaiz !
6. Commit sur branche dédiée à la fonctionnalité elle-même gérée par une seule personne
7. Quand ça fonctionne : merge dans master
- *Marnoche* : création et gestion du perso
- *Maxim* : objets du terrain + terrain
- *Nadia* :  MAJ prise de notes qui était à l'abandon (ok) + ajout matter (ok) + adapter le projet de base Master en modules (ok) + test d'implémentation d'un module Matter à l'architecture en modules d'Antho pour reproductions futures rapides (ok avec Avalanche) + orchestration de la répartition des tâches en demandant ce que chacun.e fait ;) Tic Tac... Alex style ! (hem) + MAJ Board GitLab (ok) + ce qui est cité au début de la journée du 4 mars

### 5 mars
On prend les mêmes et on nage, on nage, on nage...

### 8 mars
Bon après avoir kidnappé 1h Antho vendredi début pm, on a débloqué *(à peu près)* le démarrage sur Matter.
Mais Marnoche bloque encore.
Max avance de son côté et comprend pas mal js et Matter (on compte grave sur lui !!)

#### Nadia 
- a réussi à adapter (encoooore) non pas div à canvas mais canvas à Matter et Matter à architecture React style (modules) (ok)
- mise à dispo du squelette de base, adapté comme sus-cité. Incluant une avalanche dont on aura besoin. (ok)
- je m'informe sur le git workflow pour qu'on soit efficaces : https://www.atlassian.com/fr/git/tutorials/comparing-workflows & https://rogerdudler.github.io/git-guide/ (liens fournis par Alex) + https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow (ok)
- mise à dispo du how to pour créer une branche par module afin que chacun versionne avant de merger (on ne touche plus à Master) (ok)
- mise à jour du board (ok)

#### Etapes Git workflow proposé :
1. Vous clonez *(ou "pullez" si déjà cloné)* la master en local pour travailler sur la master à jour *(que j'ai mise en v.1.0.0 le 8/3)*, sur votre machine
2. vous créez une branche dédiée à votre partie :  
- allez sur le dossier local du projet master et "git bash here"
- pour créer la branche, il suffit de taper :
``git checkout -b nom-de-votre-feature master``
- *1 branche par feature avec un nom descriptif, par ex : personnage, animation prise, caméra, déplacements, etc.. => diviser autant que possible les features en branches pour éviter les conflits ou les repérer plus facilement*
4. vous n'avez plus qu'à travailler comme d'habitude et faire vos commits et push sur la branche
5. Si je passe la master en version plus récente, je vous tiendrai informés afin que vous en fassiez une maj locale *(ex après l'ajout d'une feature qui marche)*
6. quand une fonctionnalité est ok, vous pouvez demander une pull request. Une pull request est une discussion dédiée à une branche en particulier avant qu'on décide ensemble de sa fusion avec master.
Les pull request peuvent aussi être utilisée si besoin d'aide sur la branche en question.
7. Si besoin, on peut pull une feature en local pour aider à résoudre un problème
8. soit vous mergez vers master soit je m'en occupe

Lien des infos : https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow

### 9 mars
#### Nadia :
- Fouiller tous les snippets Matter pour trouver ceux qui pourraient nous être utiles et tenter de les paramétrer selon retours de Max et Marnoche
- Voir avec Max et Marnoche pour savoir s'ils vont avoir besoin de fonctionnalités spécifiques que je pourrais chercher pour eux
- Comme Max est sur le 1er tableau du jeu, je vais voir à créer le second au vu de ce que je trouverai sur Matter. Mon but : n'avoir rien à adapter, du pur copier coller avec ce qui est déjà proposé par Matter.
- intégrer les déplacements dans le système en modules (ok!!) et ajouter à master (ok)
- commenter les fichiers du système en module pour implémentation plus aisée pour marnoche et max (ok)
#### Max :
- continuer sur le tableau 1 et appréhender comment passer d'un tableau à l'autre avec :
``` js
  Render.lookAt(render, {
        min: { x: 0, y: 0 },
        max: { x: 800, y: 600 }
    });
```
*(prezi style, ce qu'utilisait Allan pour ses présentations zoomées/dézommées)*
#### Marnoche :
- transposer son code dans l'avalanche, afficher correctement le perso et gérer la limite de déplacement pour le perso à intégrer (qu'il ne dépasse pas de l'écran par exemple) et à lui faire faire des vrais sauts et atterrissages. Et le faire sur déplacer sur appui long plutôt que répété.... Pour le moment, il vole plus qu'autre chose xD

### 10 mars
#### Nadia
- ajout murs, porte & centrage plateforme et avalanche
- recherches
#### Marnoche
- sprite fonctionnel
#### Max
- ?

### 11 mars
#### Nadia
- réorga code avec levels (ok)
- tout remarche (ui)
- reste à régler affichage (dimensions) : à cause de la caméra, je ne peux plus utiliser windows.innerWidth et height du coup rien ne s'affiche correctement => caméra désactivée en attendant de régler ça
#### Marnoche
- recherches sur insertion chronomètre et enregistrement dans un fichier texte (et non local storage qu'on ne contrôle pas)
#### Max
- mumuse avec la création d'un nouveau sprite
- adaptation de sa prise élec au système des levels

### 12/03
- On nage on nage on nage
- Marnonche et Nadia essayons de créer un monde avec insertion objets (facile), stack (très difficile).
- Max est plutôt sur le déplacement et le nouveau sprite

### 15/03
#### TIC TAC
- Stand up du jour qui nous fait encore plus prendre conscience de notre retard par rapport à la deadline.
#### Adaptation pour rendre un jeu simple mais fonctionnel d'ici à vendredi :
- Au lieu de chacun créer son monde, on ne va en créer qu'un seul, grand. Marnoche a bien avancé sur la création de son monde : il lui sera proposé dans l'après midi (quand elle sera là) de continuer sur sa bonne lancée, avec des objets simples, pas des stacks, à moins d'en avoir le temps sur du peauffinage
- Max reste sur le peauffinage des mouvements du perso et de la camera
- Nadia passe sur la création/insertion d'un timer et l'enregistrement des scores sur fichier (pas localstorage) + page accueil et scores
#### On fait les basiques et si - ET SEULEMENT SI - on a le temps après avoir fini, on peut essayer de faire du peauffinage

# CONLUSION

- Marnoche : a pensé et créé les objets du monde après avoir bien entamé le premier sprite et les déplacements de la balle + stylisé la landing page
- Max : a créé le dernier sprite, les déplacements de la balle, et même la musique !
- Nadia : a créé la machinerie et ses commentaires - backend un jour, backend toujours ;) - le timer et le local storage + landing et scores pages et "stylisé" cette dernière

## Bien entendu, on a fait au mieux et beaucoup de choses peuvent être améliorées tant au niveau machinerie que front.

